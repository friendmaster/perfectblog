class CommentsController < ApplicationController
  respond_to :json
  before_action :authenticate_user!, except: [:index]

  def index
    article = Article.find(params[:article_id])
    comments = article.comments

    respond_with(comments) do |format|
      format.json do
        render json: comments.as_json(
            only: [:id, :user_id, :content, :created_at],
            include: [ user: { only: [:id, :username] } ]
        )
      end
    end
  end

  def create
    article = Article.find(params[:article_id])
    comment = article.comments.create(comment_params.merge!(user_id: current_user.id))

    respond_with(article, comment) do |format|
      format.json do
        render json: comment.as_json(
            only: [:id, :user_id, :content, :created_at],
            include: [ user: { only: [:id, :username] } ]
        )
      end
    end
  end

  def destroy
    comment = Comment.find(params[:id])
    respond_with(comment.destroy)
  end

  protected

  def comment_params
    params.require(:comment).permit(:content)
  end
end