class TagsController < ApplicationController
  respond_to :json

  def index
    article = Article.find(params[:article_id])
    tags = article.tags
    respond_with(tags)
  end
end