class ArticlesController < ApplicationController
  extend ActionController::Live

  respond_to :json
  before_action :authenticate_user!, except: [:index, :show]

  def index
    articles = Article.order(id: :desc)
    articles = articles.joins(taggings: :tag).where('tags.title = ?', params[:tag]) if params[:tag]

    respond_with(articles) do |format|
      format.json do
        render json: articles.as_json(
            only: [:id, :user_id, :title, :content, :created_at, :updated_at],
            include: [
                { tags: { only: [:id, :title] } },
                { user: { only: [:id, :username] } }
            ]
        )
      end
    end
  end

  def search
    search = Article.search do
      fulltext params[:text]
    end

    articles = search.results

    respond_with(articles) do |format|
      format.json do
        render json: articles.as_json(
            only: [:id, :user_id, :title, :content, :created_at, :updated_at],
            include: [
                { tags: { only: [:id, :title] } },
                { user: { only: [:id, :username] } }
            ]
        )
      end
    end
  end

  def show
    article = Article.find(params[:id])

    respond_with(article) do |format|
      format.json do
        render json: article.as_json(
            only: [:id, :user_id, :title, :content, :created_at, :updated_at],
            include: [
                { tags: { only: [:id, :title] } },
                { user: { only: [:id, :username] } }
            ]
        )
      end
    end
  end

  def create
    return unless current_user.has_role? :admin

    article = current_user.articles.create(article_params)
    article.add_tags(params[:tags])
    respond_with(article)
  end

  def update
    return unless current_user.has_role? :admin

    article = Article.find(params[:id])
    article.update(article_params)
    article.update_tags(params[:tags])
    respond_with(article)
  end

  def destroy
    return unless current_user.has_role? :admin

    article = Article.find(params[:id])
    respond_with(article.destroy)
  end

  protected

  def article_params
    params.require(:article).permit(:title, :content)
  end
end