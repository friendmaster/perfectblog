class WidgetController < FayeController
  channel '/widget' do
    monitor :subscribe do
      #Rails.logger.info "s #{current_user.inspect}"
    end

    monitor :unsubscribe do
      #Rails.logger.info "u #{current_user.inspect}"
    end

    monitor :publish do
      #Rails.logger.info "p #{current_user.inspect}"
    end
  end
end