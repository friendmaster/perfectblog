@app
  .controller 'ArticlesIndexCtrl', ($scope,Article) ->
    $scope.items = Article.query()

    $scope.deleteArticle = (index) ->
      article = $scope.items[index]

      Article.delete({ id: article.id },
        (->
          $scope.items.splice index, 1
          return
        )
      )


  .controller 'ArticlesTagsCtrl', ($scope, $routeParams, Article) ->
    $scope.tag = $routeParams.tag
    $scope.items = Article.query({ tag: $scope.tag })


  .controller 'ArticlesSearchCtrl', ($scope, $routeParams, Article) ->
    $scope.text = $routeParams.text
    $scope.items = Article.query({ id: 'search', text: $scope.text })


  .controller 'ArticlesShowCtrl', ($scope, $routeParams, Article, Tag, Comment) ->
    $scope.id = $routeParams.article_id
    $scope.article = Article.get({ id: $scope.id })
    $scope.comments = Comment.query({ article_id: $scope.id })

    $scope.deleteArticle = ->
      Article.delete({id: $scope.id}, ->
        $location.path('/')
      )


  .controller 'ArticlesEditCtrl', ($scope, $routeParams, $location, Article, Tag) ->
    $scope.id = $routeParams.id;
    $scope.article = Article.get({ id: $scope.id })

    Tag.query({ article_id: $scope.id }, (tags) ->
      $scope.tags = []
      angular.forEach(tags, (obj) ->
        $scope.tags.push {text: obj.title}
      )
    )

    $scope.$watch('tags.length', () ->
      $scope.article.tags = []
      angular.forEach($scope.tags, (obj) ->
        $scope.article.tags.push obj.text
      )
    )

    $scope.saveArticle = ->
      Article.update({ id: $scope.id }, $scope.article, (article) ->
        $location.path('/articles/' + article.id)
      )

    $scope.showArticle = ->
      $location.path('/articles/' + $scope.id)


  .controller 'ArticlesNewCtrl', ($scope, $routeParams, $location, Article) ->
    $scope.article = {
      title: ''
      description: ''
      tags: []
    }

    $scope.tags = []

    $scope.$watch('tags.length', () ->
      $scope.article.tags = []
      angular.forEach($scope.tags, (obj) ->
        $scope.article.tags.push obj.text
      )
    )

    $scope.createArticle = ->
      Article.save($scope.article, (article) ->
        $location.path('/articles/' + article.id)
      )