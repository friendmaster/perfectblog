@app
  .controller 'CommentsCtrl', ($scope, $routeParams, Comment) ->
    $scope.article_id = $routeParams.article_id;
    $scope.comments = Comment.query({article_id: $scope.article_id})

    $scope.new_comment = {
      article_id: $scope.article_id
      content: ''
    }

    $scope.createComment = ->
      Comment.save($scope.new_comment, (comment) ->
        $scope.new_comment.content = ''
        $scope.comments.push comment
      )

    $scope.deleteComment = (index) ->
      comment = $scope.comments[index]

      Comment.delete({ article_id: $scope.article_id, id: comment.id },
        (->
          $scope.comments.splice(index, 1);
          return
        )
      )
