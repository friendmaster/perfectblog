@app
  .controller 'DeviseCtrl', ($scope, $http, $auth, Faye) ->
    $scope.data = []

    Faye.client.addExtension outgoing: (message, callback) ->
      message.ext = message.ext or {}
      if $auth.userIsAuthenticated()
        message.ext =  $auth.retrieveData('auth_headers')

      callback message
      return

    setInterval(->
      myTimer()
      return
    , 1000)

    myTimer = ->
      Faye.publish "/widget", { msg: "hello" }


    Faye.subscribe "/widget", (msg) ->
      $scope.data.push msg

    # Get just once (using $q - promise)
    #$scope.data = Faye.get("/widget")

    $scope.auth = () ->
      if $auth.userIsAuthenticated()
        console.log($auth.retrieveData('auth_headers'))


  .controller 'SearchCtrl', ($scope, $location) ->
    $scope.text = ''

    $scope.search = () ->
      $location.path('/search/' + $scope.text)


  .controller 'DeviseLoginCtrl', ($scope, $auth, $location) ->
    $scope.user = {}
    $scope.errors = []

    $scope.$on 'auth:login-succes', (ev, user) ->
      $location.path('/')

    $scope.$on 'auth:login-error', (ev, reason) ->
      console.log('hello')
      $scope.errors = reason.errors

    $scope.submitLogin = ->
      console.log($auth.submitLogin($scope.user))
      return


  .controller 'DeviseRegistrationCtrl', ($scope, $auth, $location) ->
    $scope.user = {}
    $scope.errors = []

    $scope.$on 'auth:registration-email-success', (ev, user) ->
      $location.path('/login')

    $scope.$on 'auth:registration-email-error', (ev, reason) ->
      $scope.errors = reason.errors

    $scope.submitRegistration = () ->
      $auth.submitRegistration($scope.user)
      return


