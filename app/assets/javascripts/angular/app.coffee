#= require angular
#= require angular-route
#= require angular-resource
#= require angular-cookies
#= require ng-tags-input
#= require angular-faye
#= require angular-devise
#= require ng-token-auth

@app = angular.module('PerfectBlogApp', ['ngRoute', 'ngResource', 'ngTagsInput', 'faye', 'ng-token-auth'])

app.factory 'Faye', ['$faye', ($faye) ->
  $faye("/faye")
]

.config ($routeProvider) ->
  $routeProvider
    .when '/',
      templateUrl: 'assets/articles/index'
      controller: 'ArticlesIndexCtrl'
    .when '/articles/new',
      templateUrl: 'assets/articles/new'
      controller: 'ArticlesNewCtrl'
    .when '/articles/:article_id',
      templateUrl: 'assets/articles/show'
      controller: 'ArticlesShowCtrl'
    .when '/articles/:id/edit',
      templateUrl: 'assets/articles/edit'
      controller: 'ArticlesEditCtrl'
    .when '/login',
      templateUrl: 'assets/users/login'
      controller: 'DeviseLoginCtrl'
    .when '/registration',
      templateUrl: 'assets/users/registration'
      controller: 'DeviseRegistrationCtrl'
    .when '/tags/:tag',
      templateUrl: 'assets/articles/index'
      controller: 'ArticlesTagsCtrl'
    .when '/search/:text',
      templateUrl: 'assets/articles/index'
      controller: 'ArticlesSearchCtrl'
    .otherwise
      redirectTo: '/'

.config ["$httpProvider", ($httpProvider) ->
  $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content')
]

.constant 'Application',
  name: 'Perfect Blog'
  version: "0.0.1"

.run ($rootScope, $location, Application) ->
  $rootScope.application = Application
  return


@app.config ($authProvider) ->
  $authProvider.configure
    parseExpiry: (headers) ->
      (parseInt(headers["expiry"]) * 1000) or null

  return

