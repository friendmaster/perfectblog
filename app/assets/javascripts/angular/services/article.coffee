@app.factory 'Article', ($resource) ->
  $resource '/articles/:id.json', null, {
    'update': { method: 'PUT' }
    'search': { method: 'GET' }
  }