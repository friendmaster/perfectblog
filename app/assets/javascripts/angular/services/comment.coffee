@app.factory 'Comment', ($resource) ->
  $resource '/articles/:article_id/comments/:id.json', { article_id: '@article_id' }, {
    update: { method: 'PUT' }
  }