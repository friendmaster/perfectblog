@app.factory 'Tag', ($resource) ->
  $resource '/articles/:article_id/tags/:id.json', { article_id: '@article_id' }