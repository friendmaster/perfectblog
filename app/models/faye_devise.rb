class FayeDevise
  def incoming(message, callback)
    set_user_by_token(message)
    callback.call(message)
  end

  def set_user_by_token(message)
    uid        = message['ext']['uid']
    @token     = message['ext']['access-token']
    @client_id = message['ext']['client']

    return false unless @token

    @client_id ||= 'default'

    user = uid && User.find_by_uid(uid)

    if user && user.valid_token?(@token, @client_id)
      # add methods to FayeRails
      @user = user

      #FayeRails::Controller::Monitor.send :define_method, :, &block

      Rails.logger.info user.email
    end
  end

  # def update_auth_header
  #   return unless @user and @user.valid? and @client_id
  #
  #   if not DeviseTokenAuth.change_headers_on_each_request
  #     @message['ext'] = @user.build_auth_header(@token, @client_id)
  #   elsif @is_batch_request
  #     @user.extend_batch_buffer(@token, @client_id)
  #   else
  #     @message['ext'] = @user.create_new_auth_token(@client_id)
  #   end
  # end

  # private
  #
  # def is_batch_request?(user, client_id)
  #   user.tokens[client_id] and
  #       user.tokens[client_id]['updated_at'] and
  #       Time.parse(user.tokens[client_id]['updated_at']) > Time.now - DeviseTokenAuth.batch_request_buffer_throttle
  # end
end