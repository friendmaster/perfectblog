class MyMiddleware
  def initialize(app)
    @app = app #Store the app to call it down the stack
  end

  def call(env)
    status, headers, body = @app.call(env)

    if headers.has_key?('ETag')
      [status, headers, body]
    else
      parts = []
      body.each { |part| parts << part.to_s }

      headers['ETag'] = %("#{Digest::MD5.hexdigest(parts.join(''))}")

      [status, headers, parts]
    end
  end
end