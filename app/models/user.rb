class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable

  has_many :articles
  has_many :comments

  devise :timeoutable, timeout_in: 15.seconds
end
