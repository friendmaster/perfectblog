class Tag < ActiveRecord::Base
  has_many :taggings
  has_many :articles, through: :taggings

  def self.vacuum
    unused_tags = Tag.select('tags.id')
                     .joins('LEFT JOIN taggings ON taggings.tag_id = tags.id')
                     .group('tags.id')
                     .having('COUNT(taggings.id)=0')

    Rails.logger.info unused_tags

    unused_tags.each do |x|
      x.destroy
    end
  end
end
