class Article < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings
  belongs_to :user

  searchable do
    text :title, :content
  end

  validates_presence_of :user_id
  validates_presence_of :content

  after_destroy { Tag.vacuum }
  after_update  { Tag.vacuum }

  def add_tags(tags)
    return unless tags

    tags.each do |title|
      tag = Tag.find_or_create_by(title: title)
      Tagging.create(tag: tag, article: self)
    end

    self
  end

  def delete_tags(tags)
    tags.each do |x|
      tag = Tag.find_by_title(x)
      tagging = Tagging.find_by({ article: self, tag: tag })
      tagging.destroy
    end
  end

  def update_tags(new_tags)
    exists_tags = self.tags.map(&:title)
    delete_tags(exists_tags - new_tags.to_a)
    add_tags(new_tags.to_a - exists_tags)
  end
end