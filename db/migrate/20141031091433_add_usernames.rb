class AddUsernames < ActiveRecord::Migration
  def up
    users = User.where('username IS NULL')
    users.each do |user|
      user.username = 'User' + rand(1...1000).to_s
      user.save
    end
  end
end
