# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
user = User.new
user.username = 'Admin'
user.email = 'admin@example.com'
user.password = '11111111'
user.password_confirmation = '11111111'
user.save!

user.add_role :admin

