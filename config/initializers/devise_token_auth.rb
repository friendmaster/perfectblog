DeviseTokenAuth.setup do |config|
  #config.change_headers_on_each_request = true
  config.token_lifespan = 15.minutes
  #config.batch_request_buffer_throttle = 5.seconds
  #config.omniauth_prefix = "/omniauth"
end
