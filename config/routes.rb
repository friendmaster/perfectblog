Rails.application.routes.draw do

  scope :api do
    mount_devise_token_auth_for 'User', at: '/auth'
  end

  resources :articles, defaults: { format: 'json' } do
    collection do
      get 'search'
    end

    resources :comments, defaults: { format: 'json' }
    resources :tags,  only: [:index], defaults: { format: 'json' }
  end

  root 'home#index'
end
