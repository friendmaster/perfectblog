require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PerfectBlog
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.middleware.use FayeRails::Middleware, mount: '/faye', timeout: 15 do
      map '/widget' => WidgetController
      map default: :block
      add_extension(FayeDevise.new)
    end

    class MyMiddleware
      def initialize(app)
        @app = app #Store the app to call it down the stack
      end

      def call(env)
        status, headers, body = @app.call(env)

        if headers.has_key?('ETag')
          [status, headers, body]
        else
          parts = []
          body.each { |part| parts << part.to_s }

          headers['ETag'] = %("#{Digest::MD5.hexdigest(parts.join(''))}")

          [status, headers, parts]
        end
      end
    end

    config.middleware.insert_before ActionDispatch::Static, MyMiddleware

    config.middleware.delete Rack::Lock
  end
end
